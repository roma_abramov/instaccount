const mix = require('laravel-mix');

//region js
mix
    .js('resources/js/app.js', 'public/js')
;
//endregion

//region css
mix.sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false,
    })
;
//endregion

mix.extract();

if (mix.inProduction()) {
    mix
        .version()
        .disableNotifications()
    ;
} else {
    mix
        .sourceMaps()
    ;
}

