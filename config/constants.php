<?php

return [
    'chunk_size' => 10240,

    'round_precision' => 2,

    'time_limit'   => 1200,
    'memory_limit' => '1024M',
    // 'time_limit'   => 0, // unlimited
    // 'memory_limit' => '-1', // unlimited
];
