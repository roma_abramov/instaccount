<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title', __('Untitled')) | {{ config('app.name') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--
    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/4.4/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/4.4/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/4.4/assets/img/favicons/safari-pinned-tab.svg" color="#563d7c">
    <link rel="icon" href="/docs/4.4/assets/img/favicons/favicon.ico">
    <meta name="msapplication-config" content="/docs/4.4/assets/img/favicons/browserconfig.xml">
    <meta name="theme-color" content="#563d7c">
--}}

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    @stack('styles')
</head>
<body>
    <nav class="sticky-top navbar navbar-expand-md navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
                aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand" href="{{ route('home') }}">{{ config('app.name') }}</a>

        <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ Route::is('account.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('account.index') }}">{{ __('Accounts') }}</a>
                </li>
                <li class="nav-item {{ Route::is('adv.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('adv.index') }}">{{ __('Advertising') }}</a>
                </li>
                <li class="nav-item {{ Route::is('diff.index') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('diff.index') }}">{{ __('Differences') }}</a>
                </li>
            </ul>
        </div>
    </nav>

    <main class="container my-5">
        <h1>@yield('title', __('Untitled'))</h1>

        @yield('content')
    </main>

    <script src="{{ mix('/js/manifest.js') }}" defer></script>
    <script src="{{ mix('/js/vendor.js') }}" defer></script>
    <script src="{{ mix('/js/app.js') }}" defer></script>
    @stack('scripts')
</body>
</html>
