
@props(['name', 'label'])
@php
    $name = $name ?? 'date';
    $id = $name ?? Str::random(8);
@endphp

<div class="form-group">
    <label for="{{ $id }}">{{ $label ?? __('Date') }}:</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">📅</span>
        </div>
        <input type="date" name="{{ $name }}" id="{{ $id }}"
               class="form-control @error($name) is-invalid @enderror"
               value="{{ old($name) ?? date('Y-m-d') }}" required autocomplete="off" >

        @error($name)
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
