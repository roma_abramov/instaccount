
@props(['name', 'source', 'label'])

@php
    $name = $name ?? 'account';
    $id = $name ?? Str::random(8);
@endphp

<div class="form-group">
    <label for="{{ $id }}">{{ $label ?? __('Account') }}:</label>
    <div class="input-group autocomplete align-items-stretch">
        <div class="input-group-prepend">
            <span class="input-group-text">@</span>
        </div>
        <input type="text" name="{{ $name }}" value="{{ old($name) }}" id="{{ $id }}"
               class="form-control rounded-right @error($name) is-invalid @enderror"
               placeholder="enter account name" autocomplete="off"
               required data-source='@json($source)' >

        @error($name)
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
