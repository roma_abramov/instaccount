
@props(['route', 'errors'])

<div class="col-md-6">
    <form action="{{ route($route) }}" method="POST" enctype="multipart/form-data" class="mt-3" novalidate>
        @csrf
        {{ $slot }}
        <button class="btn btn-primary mt-3" type="submit">{{ __('Compare') }}</button>
    </form>

    @if ($message = Session::get('success'))
        @if (!empty($successSlot))
            {{ $successSlot }}
        @else
            <div class="alert alert-success my-3">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
        @endif
    @endif
</div>
