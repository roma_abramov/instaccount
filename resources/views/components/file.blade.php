
@props(['name', 'label'])

@php
    $name = $name ?? 'file';
    $id = $name ?? Str::random(8);
@endphp

{{ old($name) }}
<div class="form-group">
    <label>{{ $label ?? __('File') }}:</label>
    <div class="custom-file">
        <input type="file" name="{{ $name }}" id="{{ $id }}" {{ $attributes }}
               class="custom-file-input @error($name) is-invalid @enderror"
               value="{{ old($name) }}" placeholder="{{ __('Choose file...') }}" autocomplete="off">
        <label class="custom-file-label" for="{{ $id }}">{{ old($name) ?? __('Choose file...') }}</label>

        @error($name)
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
