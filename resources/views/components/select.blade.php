
@props(['name', 'label', 'source'])

@php
    $name = $name ?? 'account';
    $id = $name ?? Str::random(8);
    $source = json_decode($source ?? '', true);
@endphp

<div class="form-group">
    <label for="{{ $id }}">{{ $label ?? __('Account') }}:</label>
    <div class="input-group autocomplete align-items-stretch">
        <div class="input-group-prepend">
            <span class="input-group-text">@</span>
        </div>
        <select name="{{ $name }}" id="{{ $id }}" {{ $attributes }}
                class="custom-select @error($name) is-invalid @enderror">
            <option value="">Open this select menu</option>

            @if (!empty($source))
            @foreach ($source as $item)
            <option value="{{ $item['name'] }}">{{ $item['name'] }}</option>
            @endforeach>
            @endif
        </select>

        @error($name)
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
