
@props(['name', 'label', 'files'])

@if (!empty($files))

@php
    $name = $name ?? 'select';
    $id = $name ?? Str::random(8);
    $files = json_decode($files ?? '', true);
@endphp

<div class="form-group">
    <label for="{{ $id }}">{{ $label ?? __('Files') }}:</label>
    <select name="{{ $name }}" id="{{ $id }}" {{ $attributes }}
            class="custom-select @error($name) is-invalid @enderror">
        <option value="">Open this select menu</option>
        @foreach ($files as $file)
        <option value="{{ $file['filename'] }}">{{ $file['name'] }}</option>
        @endforeach
    </select>
    @error($name)
    <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>

@endif
