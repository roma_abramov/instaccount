@extends('layouts.app')

@section('title', 'Сравнить подписчиков по файлам')

@section('content')
<x-form route="diff.store">
    <x-file name="newFile1" :label="__('New file 1')" />
    <x-select-file name="oldFile1" :label="__('Old file 1')" :files='$files' />
    <x-file name="newFile2" :label="__('New file 2')" />
    <x-select-file name="oldFile2" :label="__('Old file 2')" :files='$files' />
</x-form>
@endsection
