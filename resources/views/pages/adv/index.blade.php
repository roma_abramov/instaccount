@extends('layouts.app')

@section('title', 'Сравнить подписчиков с рекламным аккаунтом')

@section('content')
<x-form route="adv.store">
    <x-select :source='$accounts' />
    <x-autocomplete name="advAccount" :source='$advAccounts' :label="__('Advertising Account')" />
    <x-file />
    <x-date />
</x-form>
@endsection
