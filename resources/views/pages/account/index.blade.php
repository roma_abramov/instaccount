@extends('layouts.app')

@section('title', 'Обновить подписчиков')

@section('content')
<x-form route="account.store">
    <x-autocomplete :source="$accounts" />
    <x-file />
    <x-date />
</x-form>
@endsection
