<?php

declare(strict_types = 1);

namespace MAGarif;

use App\Models\History;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

use App\Models\Account;

/**
 * Class Comparator
 * @package MAGarif
 */
class Comparator2
{
    /** @var Carbon Дата */
    protected $date;
    /** @var Account Аккаунт */
    protected $account;
    /** @var Account Аккаунт */
    protected $advAccount;

    /**
     * FileParser constructor.
     *
     * @param  Account  $account
     * @param  Account  $advAccount
     * @param  Carbon   $date
     */
    public function __construct(Account $account, Account $advAccount, Carbon $date)
    {
        $this->date       = $date;
        $this->account    = $account;
        $this->advAccount = $advAccount;
    }

    /**
     * Сравниваем файл и личный аккант
     *
     * @param  string  $file
     *
     * @return void
     */
    public function compare(string $file): void
    {
        $followed   = collect();
        $unfollowed = collect();
        $chunkSize  = config('constants.chunk_size', 256);

        $fileFollowers  = FileParser::getContent($file)->getIterator();
        $dbFollowers    = DBHelper::getFollowers($this->account, $this->date)->getIterator();
        $dbAdvFollowers = DBHelper::getFollowers($this->advAccount, $this->date)->getIterator();
        while ($dbFollowers->valid() && $fileFollowers->valid()) {
            $fromDb   = $dbFollowers->current();
            $fromFile = $fileFollowers->current();
            switch ($fromDb <=> $fromFile) {
                case 1: // подписка (в базе нет, в файле есть)
                    $followed->add($fromFile);
                    if ($followed->count() === $chunkSize) {
                        $this->addFollowers($followed);
                    }
                    $fileFollowers->next();
                    break;
                case -1: // отписка (в базе есть, в файле нет)
                    $unfollowed->add($fromDb);
                    if ($unfollowed->count() === $chunkSize) {
                        $this->removeFollowers($unfollowed);
                    }
                    $dbFollowers->next();
                    break;
                default: // совпадение (в базе есть, в файле есть)
                    $dbFollowers->next();
                    $fileFollowers->next();
                    break;
            }
        }

        // подписка (оставшиеся в файле)
        while ($fileFollowers->valid()) {
            $followed->add($fileFollowers->current());
            if ($followed->count() === $chunkSize) {
                $this->addFollowers($followed);
            }
            $fileFollowers->next();
        }
        if ($followed->count()) {
            $this->addFollowers($followed);
        }

        // отписка (оставшиеся в базе)
        while ($dbFollowers->valid()) {
            $unfollowed->add($dbFollowers->current());
            if ($unfollowed->count() === $chunkSize) {
                $this->removeFollowers($unfollowed);
            }
            $dbFollowers->next();
        }
        if ($unfollowed->count()) {
            $this->removeFollowers($unfollowed);
        }
    }

    /**
     * Добавляем подписчиков и истторию
     *
     * @param  Collection  $followers
     *
     * @return void
     */
    public function addFollowers(Collection &$followers): void
    {
        // подписавшиеся
        DBHelper::addAccountFollowers($this->account, $followers);
        DBHelper::addHistory($this->account, $followers, $this->date, History::ACTION_FOLLOWED);

        $followers = collect();
    }

    /**
     * Удаляем подписчиков и истторию
     *
     * @param  Collection  $followers
     *
     * @return void
     */
    public function removeFollowers(Collection &$followers): void
    {
        // отписавшиеся
        DBHelper::removeAccountFollowers($this->account, $followers);
        DBHelper::addHistory($this->account, $followers, $this->date, History::ACTION_UNFOLLOWED);

        $followers = collect();
    }
}
