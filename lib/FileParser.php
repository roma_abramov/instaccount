<?php

declare(strict_types = 1);

namespace MAGarif;

use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Validator;
use Illuminate\Support\LazyCollection;
// use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Storage;

use App\Models\File;
use App\Models\Account;

/**
 * Class FileParser
 * @package MAGarif
 */
final class FileParser
{
    /**
     * Сохраняем картинку
     *
     * @param  UploadedFile  $file
     * @param  array|null    $nameParts
     *
     * @return string
     */
    public static function save(UploadedFile $file, ?array $nameParts = null): string
    {
        $filepath = $file->store($nameParts !== null ? 'files' : 'diff');
        self::sort($filepath);

        if ($nameParts !== null) {
            File::create(['name' => self::getFilename($nameParts), 'filename' => $filepath]);
        }

        return $filepath;
    }

    /**
     * @param  string       $from
     * @param  string|null  $to
     */
    public static function sort(string $from, ?string $to = null): void
    {
        #todo: Возможно, дыра в безопасности.
        // $process = new Process(['sort', storage_path('app/'. $from)]);
        // $process = new Process(['cat '. $from .' | sort | uniq > '. $from], storage_path('app/'));
        // $process->setTimeout(3600);
        // $process->run();
        Storage::put(
            $to ?? $from,
            shell_exec('sort '. storage_path('app/'. $from))
        );
    }

    /**
     * Получить имя файла
     *
     * @param  array  $parts
     *
     * @return string
     */
    public static function getFilename(array $parts): string
    {
        $filename = collect();
        foreach ($parts as $part) {
            if ($part instanceof Account) {
                $filename->add($part->name);
            } elseif ($part instanceof Carbon) {
                $filename->add($part->format('Y_m_d'));
            } else {
                $filename->add($part);
            }
        }

        return $filename->join('_');
    }

    /**
     * Ленивое чтение данных из файла
     *
     * @param  string  $filepath
     *
     * @return LazyCollection
     */
    public static function getContent(string $filepath): LazyCollection
    {
        return LazyCollection::make(
            static function () use ($filepath): iterable {
                $prev   = null;
                $handle = fopen(storage_path('app/'. $filepath), 'rb');
                while ($line = fgets($handle)) {
                    /**
                     * todo: Добавить проверку, чтоб не попала из файла уязвимость
                     * отправлять на почту или куда-то в лог писать, например
                     * или выводить на странице
                     */
                    $line = trim($line);
                    if (!empty($line) && $line !== $prev) {
                        yield $line;
                    }
                    $prev = $line;
                }
            }
        );
    }

    /**
     * Валидация файла
     *
     * @param  Validator  $validator
     * @param  array      $nameParts
     *
     * @return void
     */
    public static function validate(Validator $validator, array $nameParts): void
    {
        $validator->after(
            static function (Validator $validator) use ($nameParts) {
                if (File::where('name', '=', self::getFilename($nameParts))->exists()) {
                    $validator->errors()->add(
                        'account',
                        __('File for account by date has already been uploaded')
                    );
                }
            }
        );
    }
}
