<?php

declare(strict_types = 1);

namespace MAGarif;

/**
 * Class Comparator3
 * @package MAGarif
 */
class Comparator3
{
    /**
     * Сравниваем файл1 и файл2
     *
     * @param  string  $file1
     * @param  string  $file2
     *
     * @return array
     */
    public static function compare(string $file1, string $file2): array
    {
        $count1 = $count2 = $matches = 0;
        $file1Followers = FileParser::getContent($file1)->getIterator();
        $file2Followers = FileParser::getContent($file2)->getIterator();

        while ($file1Followers->valid() && $file2Followers->valid()) {
            $fromFile1 = $file1Followers->current();
            $fromFile2 = $file2Followers->current();
            switch ($fromFile1 <=> $fromFile2) {
                case 1: // в файле1 нет, в файле2 есть
                    $file2Followers->next();
                    ++$count2;
                    break;
                case -1: // в файле1 есть, в файле2 нет
                    $file1Followers->next();
                    ++$count1;
                    break;
                default: // совпадение
                    ++$matches;
                    $file1Followers->next();
                    ++$count1;
                    $file2Followers->next();
                    ++$count2;
                    break;
            }
        }

        // оставшиеся в файле1
        while ($file1Followers->valid()) {
            $file1Followers->next();
            ++$count1;
        }

        // оставшиеся в файле2
        while ($file2Followers->valid()) {
            $file2Followers->next();
            ++$count2;
        }

        $precision = config('constants.round_precision', 2);
        return [
            'diff1' => round(100 * $matches / $count1, $precision),
            'diff2' => round(100 * $matches / $count2, $precision),
        ];
    }
}
