<?php

declare(strict_types = 1);

namespace MAGarif;

use App\Models\History;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
// use Illuminate\Support\Facades\Schema;
// use Illuminate\Database\Schema\Blueprint;

use App\Models\Account;

/**
 * Class Comparator
 * @package MAGarif
 */
class Comparator1
{
    /** @var Carbon Дата */
    protected $date;
    /** @var Account Аккаунт */
    protected $account;

    /**
     * FileParser constructor.
     *
     * @param  Account       $account
     * @param  Carbon        $date
     */
    public function __construct(Account $account, Carbon $date)
    {
        $this->date    = $date;
        $this->account = $account;
    }

    /**
     * Сравниваем файл и личный аккант
     *
     * @param  string  $file
     *
     * @return void
     */
    public function compare(string $file): void
    {
        $followedCount = $unfollowedCount = 0;
        $followed      = $unfollowed      = collect();
        $chunkSize     = config('constants.chunk_size', 256);
        $fileFollowers = FileParser::getContent($file)->getIterator();
        $dbFollowers   = DBHelper::getFollowers($this->account, $this->date)->getIterator();
        while ($dbFollowers->valid() && $fileFollowers->valid()) {
            $fromDb   = $dbFollowers->current();
            $fromFile = $fileFollowers->current();
            switch ($fromDb <=> $fromFile) {
                case 1: // подписка (в базе нет, в файле есть)
                    $followed->add($fromFile);
                    if (++$followedCount === $chunkSize) {
                        $this->addFollowers($followed);
                        $followed = collect();
                        $followedCount = 0;
                    }
                    $fileFollowers->next();
                    break;
                case -1: // отписка (в базе есть, в файле нет)
                    $unfollowed->add($fromDb);
                    if (++$unfollowedCount === $chunkSize) {
                        $this->removeFollowers($unfollowed);
                        $unfollowed = collect();
                        $unfollowedCount = 0;
                    }
                    $dbFollowers->next();
                    break;
                default: // совпадение (в базе есть, в файле есть)
                    $fileFollowers->next();
                    $dbFollowers->next();
                    break;
            }
        }

        // подписка (оставшиеся в файле)
        while ($fileFollowers->valid()) {
            $followed->add($fileFollowers->current());
            if (++$followedCount === $chunkSize) {
                $this->addFollowers($followed);
                $followed = collect();
                $followedCount = 0;
            }
            $fileFollowers->next();
        }
        unlink($fileFollowers);
        if ($followedCount) {
            $this->addFollowers($followed);
        }
        unlink($followed);

        // отписка (оставшиеся в базе)
        while ($dbFollowers->valid()) {
            $unfollowed->add($dbFollowers->current());
            if (++$unfollowedCount === $chunkSize) {
                $this->removeFollowers($unfollowed);
                $unfollowed = collect();
                $unfollowedCount = 0;
            }
            $dbFollowers->next();
        }
        unlink($dbFollowers);
        if ($unfollowedCount) {
            $this->removeFollowers($unfollowed);
        }
        unlink($unfollowed);
    }

    /**
     * Добавляем подписчиков и истторию
     *
     * @param  Collection  $followers
     *
     * @return void
     */
    public function addFollowers(Collection $followers): void
    {
        DBHelper::addFollowers($followers);

        // подписавшиеся
        DBHelper::addAccountFollowers($this->account, $followers);
        DBHelper::addHistory($this->account, $followers, $this->date, History::ACTION_FOLLOWED);
    }

    /**
     * Удаляем подписчиков и истторию
     *
     * @param  Collection  $followers
     *
     * @return void
     */
    public function removeFollowers(Collection $followers): void
    {
        // отписавшиеся
        DBHelper::removeAccountFollowers($this->account, $followers);
        DBHelper::addHistory($this->account, $followers, $this->date, History::ACTION_UNFOLLOWED);
    }
}
