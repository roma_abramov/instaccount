<?php

declare(strict_types = 1);

namespace MAGarif;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\LazyCollection;
use Illuminate\Database\Query\Builder;

use App\Models\History;
use App\Models\Account;
use App\Models\Follower;

final class DBHelper
{
    /**
     * Добавляем новые аккаунты подписчиков
     *
     * @param  Collection  $followers
     *
     * @return void
     */
    public static function addFollowers(Collection $followers): void
    {
        Follower::insertOrIgnore($followers->map(
            static function (string $name): array {
                return ['name' => $name];
            }
        )->toArray());
    }

    /**
     * Строка с плейсхолдерами
     *
     * @param  Collection  $values
     *
     * @return string
     */
    public static function getInPlaceholders(Collection $values): string
    {
        return collect()->pad($values->count(), '?')->join(', ');
    }

    /**
     * Добавляем связь подписчика с аккаунтом
     *
     * @param  Account     $account
     * @param  Collection  $followers
     *
     * @return void
     */
    public static function addAccountFollowers(Account $account, Collection $followers): void
    {
        DB::insert('
INSERT INTO account_follower (follower_id, account_id)
SELECT id AS follower_id, (?)::bigint AS account_id
FROM followers
WHERE name IN ('. self::getInPlaceholders($followers) .')
            ;',
            array_merge([$account->id], $followers->toArray())
        );
    }

    /**
     * Удалить связь подписчика с аккаунтом
     *
     * @param  Account     $account
     * @param  Collection  $followers
     *
     * @return void
     */
    public static function removeAccountFollowers(Account $account, Collection $followers): void
    {
        DB::delete('
DELETE FROM account_follower
WHERE account_id = (?)::INT
    AND follower_id IN (
        SELECT id FROM followers WHERE name IN (' . self::getInPlaceholders($followers) . ')
    )
            ;',
            array_merge([$account->id], $followers->toArray())
        );
    }

    /**
     * Добавляем историю связи подписчика с аккаунтом
     *
     * @param  Account     $account
     * @param  Collection  $followers
     * @param  Carbon      $date
     * @param  string      $action
     *
     * @return void
     */
    public static function addHistory(Account $account, Collection $followers, Carbon $date, string $action): void
    {
        DB::insert('
INSERT INTO histories (follower_id, account_id, date, action)
SELECT id AS follower_id, (?)::INT AS account_id, (?)::DATE AS date, (?)::VARCHAR AS action
FROM followers
WHERE name IN ('. self::getInPlaceholders($followers) .')
            ;',
            array_merge([$account->id, $date->toDateString(), $action], $followers->toArray())
        );
    }

    /**
     * Подписчики аккаунта на конкретную дату
     *
     * @param  Account  $account
     * @param  Carbon   $date
     *
     * @return LazyCollection
     */
    public static function getFollowers(Account $account, Carbon $date): LazyCollection
    {
        /*
        select "followers"."name"
        from "followers"
            inner join (
                select "follower_id",
                       rank() OVER (PARTITION BY account_id, follower_id ORDER BY date DESC) as "rank"
                from "histories"
                where "account_id" = ? and "date" = ?
            ) as "history"
            on "followers"."id" = "history"."follower_id"
        where "history"."rank" = ? and "history"."action" = ?
        order by "followers"."name"
        */

        $fromHistory = History::query()
            ->select(
                'follower_id', 'action',
                DB::raw('rank() OVER (PARTITION BY account_id, follower_id ORDER BY date DESC) as rank')
            )
            ->where('account_id', '=', $account->id)
            ->where('date', '<=', $date->toDateString())
        ;

        $followers =  Follower::query()
            ->select('name')
            ->joinSub($fromHistory, 'history', static function (Builder $join) {
                $join->on('followers.id', '=', 'history.follower_id')
                    ->where('rank', '=', 1)
                    ->where('action', '=', 'FOLLOWED');
            })
            ->orderBy('name')
        ;

        return $followers->cursor()->pluck('name');
    }
}
