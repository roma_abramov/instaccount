<?php

declare(strict_types = 1);

namespace App\Http\Requests;

use App\Models\Account;
use App\Models\File;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Validation\Validator;

use MAGarif\FileParser;

class AdvAccountRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'account'    => 'required|max:30|regex:/^[\w\.-]+$/i',
            'advAccount' => 'required|max:30|regex:/^[\w\.-]+$/i',
            'file'       => 'required|file|mimes:txt',
            'date'       => 'required|before_or_equal:now|date_format:Y-m-d',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required'    => __('A :attribute is required'),
            'file'        => __('A :attribute must be file'),
            'max'         => __('A :attribute max size :max'),
            'date_format' => __('A :attribute format incorrect :date_format'),

            'account.regex'        => __('A :attribute must be alphabet, num, - and _'),
            'advAccount.regex'     => __('A :attribute must be alphabet, num, - and _'),
            'date.before_or_equal' => __('A :attribute must be before or equal now'),
            'file.mimes'           => __('A :attribute mimes must be .txt'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  Validator  $validator
     *
     * @return void
     *
     * @throws Exception
     */
    public function withValidator(Validator $validator): void
    {
        $validator->after(
            static function (Validator $validator) {
                $account = Account::first(['type' => Account::TYPE_SELF, 'name' => $this->input('account')]);
                if ($account->exists()) {
                    $validator->errors()->add(
                        'account',
                        __('Account not found')
                    );
                }
            }
        );
        FileParser::validate($validator, [
            $this->input('account'),
            $this->input('advAccount'),
            new Carbon($this->input('date'))
        ]);
    }
}
