<?php

declare(strict_types = 1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SlowAction
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  Closure  $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        ini_set('memory_limit', config('constants.memory_limit'));
        set_time_limit(config('constants.time_limit'));

        DB::connection()->disableQueryLog();

        $response = $next($request);

        DB::connection()->enableQueryLog();

        return $response;
    }
}
