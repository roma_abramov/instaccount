<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\AdvAccountRequest;
use Exception;

use Illuminate\View\View;
use Illuminate\Support\Carbon;
use Illuminate\Http\RedirectResponse;

use App\Models\Account;
use App\Http\Requests\AccountRequest;

use MAGarif\FileParser;
use MAGarif\Comparator2;

class AdvController extends Controller
{
    /** @var string Папка с щаблонами */
    public const VIEW_PATH = 'pages.adv.';

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('slow')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    final public function index(): View
    {
        return view(
            self::VIEW_PATH .'index',
            [
                'accounts' => Account::self()->get(),
                'advAccounts' => Account::adv()->get(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AdvAccountRequest  $request
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    final public function store(AdvAccountRequest $request): RedirectResponse
    {
        $request->validated();

        $date       = new Carbon($request->input('date'));
        $account    = Account::first([
            'type' => Account::TYPE_SELF,
            'name' => $request->input('account'),
        ]);
        $advAccount = Account::firstOrCreate([
            'type' => Account::TYPE_ADV,
            'name' => $request->input('advAccount'),
        ]);
        $file       = FileParser::save($request->file('file'), [$advAccount, $account, $date]);
        (new Comparator2($account, $date))->compare($file);

        return redirect()->route('account.index')
            ->with('success', __('File processed successfully.'));
    }
}
