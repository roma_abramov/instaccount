<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use Exception;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

use App\Models\File;

use MAGarif\FileParser;
use MAGarif\Comparator3;

class DiffController extends Controller
{
    /** @var string Папка с щаблонами */
    public const VIEW_PATH = 'pages.diff.';

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('slow')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    final public function index(): View
    {
        return view(
            self::VIEW_PATH .'index',
            [
                'files' => File::all(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    final public function store(Request $request): RedirectResponse
    {
        $request->validate(
            [
                'newFile1' => 'nullable|file|mimes:txt',
                'newFile2' => 'nullable|file|mimes:txt',
                'oldFile1' => 'exclude_unless:file1,null|string',
                'oldFile2' => 'exclude_unless:file2,null|string',
            ]
        );
        $delete = collect();

        // файл1
        if ($request->hasFile('newFile1')) {
            $file1 = FileParser::save($request->file('newFile1'));
            $delete->add($file1);
        } else {
            $select1 = $request->input('oldFile1');
            $file1 = Storage::exists($select1) ? $select1 : '';
        }

        // файл2
        if ($request->hasFile('newFile2')) {
            $file2 = FileParser::save($request->file('newFile2'));
            $delete->add($file2);
        } else {
            $select2 = $request->input('oldFile2');
            $file2 = Storage::exists($select2) ? $select2 : '';
        }

        $diff = Comparator3::compare($file1, $file2);

        // удаляем загруженные файлы
        Storage::delete($delete->toArray());

        return redirect()->route('diff.index')
            ->with('success', __('Result: File1 matches :diff1%, File2 matches :diff2%', $diff));
    }
}
