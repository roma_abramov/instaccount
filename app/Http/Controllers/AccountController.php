<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use Exception;

use Illuminate\View\View;
use Illuminate\Support\Carbon;
use Illuminate\Http\RedirectResponse;

use App\Models\Account;
use App\Http\Requests\AccountRequest;

use MAGarif\FileParser;
use MAGarif\Comparator1;

class AccountController extends Controller
{
    /** @var string Папка с щаблонами */
    public const VIEW_PATH = 'pages.account.';

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('slow')->only('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    final public function index(): View
    {
        return view(
            self::VIEW_PATH .'index',
            [
                'accounts' => Account::self()->get('name'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AccountRequest  $request
     *
     * @return RedirectResponse
     *
     * @throws Exception
     */
    final public function store(AccountRequest $request): RedirectResponse
    {
        $request->validated();

        $date    = new Carbon($request->input('date'));
        $account = Account::firstOrCreate([
            'type' => Account::TYPE_SELF,
            'name' => $request->input('account'),
        ]);
        $file    = FileParser::save($request->file('file'), [$account, $date]);
        (new Comparator1($account, $date))->compare($file);

        return redirect()->route('account.index')
            ->with('success', __('File processed successfully.'));
    }
}
