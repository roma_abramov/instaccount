<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class History
 * @package App\Models
 *
 * @property string $name
 * @property string $filename
 *
 * @mixin Builder
 */
class File extends Model
{
    /** @var array Доступные поля */
    protected $fillable = ['name', 'filename'];
    /** @var bool Автоматические даты */
    public $timestamps  = false;
}
