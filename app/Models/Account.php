<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Account
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 * @property string $type
 *
 * @method static self(): Builder
 * @method static adv(): Builder
 *
 * @mixin Builder
 */
class Account extends Model
{
    /** @var string Значение поля "Тип" для Собственного аккаунта */
    public const TYPE_SELF = 'SELF';
    /** @var string Значение поля "Тип" для Рекламного аккаунта */
    public const TYPE_ADV  = 'ADV';

    /** @var array Доступные поля */
    protected $fillable = ['name', 'type'];
    /** @var bool Автоматические даты */
    public $timestamps  = false;

    /**
     * @return BelongsToMany
     */
    final public function followers(): BelongsToMany
    {
        return $this->belongsToMany(Follower::class);
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    final public function scopeSelf(Builder $query): Builder
    {
        return $query->where('type', '=', self::TYPE_SELF);
    }

    /**
     * @param  Builder  $query
     *
     * @return Builder
     */
    final public function scopeAdv(Builder $query): Builder
    {
        return $query->where('type', '=', self::TYPE_ADV);
    }
}
