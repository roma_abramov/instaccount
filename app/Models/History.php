<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class History
 * @package App\Models
 *
 * @property Carbon $date
 * @property string $action
 * @property int    $account_id
 * @property int    $follower_id
 *
 * @mixin Builder
 */
class History extends Model
{
    /** @var int Значение поля "Действие" для Подписки аккаунта */
    public const ACTION_FOLLOWED   = 'FOLLOWED';
    /** @var int Значение поля "Действие" для Отписки аккаунта */
    public const ACTION_UNFOLLOWED = 'UNFOLLOWED';

    /** @var bool Автоматические даты */
    public $timestamps  = false;
    /** @var string[] Поля даты */
    protected $dates    = ['date'];
}
