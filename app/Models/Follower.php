<?php

declare(strict_types = 1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Follower
 * @package App\Models
 *
 * @property int    $id
 * @property string $name
 *
 * @mixin Builder
 */
class Follower extends Model
{
    /** @var array Доступные поля */
    protected $fillable = ['name'];
    /** @var bool Автоматические даты */
    public $timestamps  = false;

    /**
     * @return BelongsToMany
     */
    final public function accounts(): BelongsToMany
    {
        return $this->belongsToMany(Account::class);
    }
}
