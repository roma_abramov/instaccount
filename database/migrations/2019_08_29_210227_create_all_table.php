<?php
declare(strict_types=1);

use App\Models\Account;
use App\Models\History;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('accounts', static function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->enum('type', [ Account::TYPE_SELF, Account::TYPE_ADV ]);
        });
        Schema::create('followers', static function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
        });
        Schema::create('account_follower', static function (Blueprint $table) {
            $table->foreignId('account_id')->constrained()->onDelete('cascade');
            $table->foreignId('follower_id')->constrained()->onDelete('cascade');
            $table->unique(['account_id', 'follower_id']);
        });

        Schema::create('histories', static function (Blueprint $table) {
            $table->date('date');
            $table->enum('action', [History::ACTION_FOLLOWED, History::ACTION_UNFOLLOWED]);
            $table->foreignId('account_id')->constrained()->onDelete('cascade');
            $table->foreignId('follower_id')->constrained()->onDelete('cascade');
            // $table->unique(['account_id', 'follower_id', 'date', 'action']);
        });

        Schema::create('files', static function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('filename')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('files');

        Schema::dropIfExists('history');

        Schema::dropIfExists('account_follower');
        Schema::dropIfExists('followers');
        Schema::dropIfExists('accounts');
    }
}
